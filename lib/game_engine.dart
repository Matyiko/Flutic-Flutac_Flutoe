import 'package:flutter/material.dart';
import 'dart:math';

const cellSize = 140.0;

enum CellState { x, o, empty }

var wins = 0;
var draws = 0;
var loses = 0;

final board = [
  for (int i = 0; i != 3; i++)
    [
      for (int j = 0; j != 3; j++) CellState.empty,
    ],
];
var isPlayerFirst = true;
var playerState = CellState.x;
var botState = CellState.o;

var isRunning = true;
var isPlayerNext = true;

var isInverted = false;

void main() {
  runApp(
    const MaterialApp(
      title: 'Flutic-Flutac-Flutoe',
      home: SafeArea(
        child: InvertibleScaffoldWidget(),
      ),
    ),
  );
}

class GameWidget extends StatefulWidget {
  const GameWidget({Key? key}) : super(key: key);

  @override
  GameState createState() => GameState();
}

class GameState extends State<GameWidget> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CounterWidget(number: wins, color: Colors.green),
            CounterWidget(number: draws, color: Colors.black),
            CounterWidget(number: loses, color: Colors.red),
          ],
        ),
        Table(
          border: TableBorder.all(),
          children: [
            for (int i = 0; i != board.length; i++)
              TableRow(
                children: [
                  for (int j = 0; j != board[i].length; j++)
                    CellWidget(
                        board[i][j],
                        isRunning && isPlayerNext
                            ? () {
                                setState(() {
                                  board[i][j] = playerState;
                                  if (!checkWin()) {
                                    isPlayerNext = false;
                                    Future.delayed(Duration(milliseconds: Random().nextInt(500) + 500), bot).then((_) {
                                      setState(() {
                                        isPlayerNext = true;
                                        checkWin();
                                      });
                                    });
                                  }
                                });
                              }
                            : () {})
                ],
              ),
          ],
        ),
        TextButton(
          onPressed: () {
            isRunning && isPlayerNext || !isRunning
                ? setState(() {
                    for (final row in board) {
                      for (int i = 0; i != row.length; i++) {
                        row[i] = CellState.empty;
                      }
                    }
                    isRunning = true;
                    final tmp = playerState;
                    playerState = botState;
                    botState = tmp;
                    isPlayerFirst = !isPlayerFirst;
                    if (!isPlayerFirst) {
                      setState(() {
                        bot();
                      });
                    }
                  })
                : () {};
          },
          child: const Text('Restart', style: TextStyle(fontSize: cellSize / 2.0)),
        ),
      ],
    );
  }

  void bot() {
    while(true) {
      int i = Random().nextInt(3);
      int j = Random().nextInt(3);
      if (board[i][j] == CellState.empty) {
        board[i][j] = botState;
        return;
      }
    }
  }

  bool checkWin() {
    final result = check();
    if (result != null) {
      final IconData icon;
      final String title;
      final Color color;
      if (result == playerState) {
        wins++;
        icon = Icons.emoji_events_outlined;
        title = 'Player won';
        color = Colors.green;
      } else if (result == botState) {
        loses++;
        icon = Icons.smart_toy_outlined;
        title = 'Player lost';
        color = Colors.red;
      } else {
        draws++;
        icon = Icons.thumbs_up_down_outlined;
        title = 'Draw';
        color = Colors.black;
      }
      isRunning = false;
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: color,
          content: ListTile(
            leading: Icon(icon, color: Colors.white),
            title: Text(title, style: const TextStyle(color: Colors.white)),
          ),
        ),
      );
    }
    return result != null;
  }

  CellState? check() {
    for (final state in [CellState.x, CellState.o]) {
      if (board[0][0] == state && board[1][1] == state && board[2][2] == state) {
        return state;
      }
      if (board[2][0] == state && board[1][1] == state && board[0][2] == state) {
        return state;
      }
      for (int i = 0; i != board.length; i++) {
        if (board[i][0] == state && board[i][1] == state && board[i][2] == state) {
          return state;
        }
        if (board[0][i] == state && board[1][i] == state && board[2][i] == state) {
          return state;
        }
      }
    }
    for (int i = 0; i != board.length; i++) {
      for (int j = 0; j != board[i].length; j++) {
        if (board[i][j] == CellState.empty) {
          return null;
        }
      }
    }
    return CellState.empty;
  }
}

class CellWidget extends StatelessWidget {
  final CellState cellState;
  final void Function() onTap;

  const CellWidget(this.cellState, this.onTap, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (cellState == CellState.empty) {
      return SizedBox(
        width: cellSize,
        height: cellSize,
        child: GestureDetector(
          onTap: onTap,
        ),
      );
    } else {
      return Icon(
        cellState == CellState.x ? Icons.close : Icons.circle_outlined,
        size: cellSize,
      );
    }
  }
}

class CounterWidget extends StatelessWidget {
  final int number;
  final Color color;

  const CounterWidget({required this.number, required this.color, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: Center(
          child: Text(
            '$number',
            style: const TextStyle(fontSize: cellSize / 2, color: Colors.white),
          ),
        ),
        color: color,
      ),
    );
  }
}

class InvertibleScaffoldWidget extends StatefulWidget {
  const InvertibleScaffoldWidget({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => InvertibleScaffoldState();
}

class InvertibleScaffoldState extends State<InvertibleScaffoldWidget> {
  @override
  Widget build(BuildContext context) {
    final child = Scaffold(
      appBar: AppBar(
        title: const Text('Flutic-Flutac-Flutoe'),
      ),
      body: const GameWidget(),
      drawer: Drawer(
        child: Column(
          children: [
            const DrawerHeader(
              child: Icon(Icons.category_outlined, size: cellSize),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Text('Invert colors'),
                Switch(value: isInverted, onChanged: (value) => setState(() => isInverted = value)),
              ],
            ),
          ],
        ),
      ),
    );
    if (isInverted) {
      return ColorFiltered(
          colorFilter: const ColorFilter.mode(
            Colors.white,
            BlendMode.difference,
          ),
          child: child);
    }
    return child;
  }
}
